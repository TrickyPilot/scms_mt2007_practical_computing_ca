# SCMS_MT2007_practical_computing_ca

the project contains a python notebook with necessary codes for practical computing CA, and an html file of the same file

## Contents

Codes of exercise 1, 2, 4 and 5 are present in the same jupyter notebook

All libraries required are imported at the start itself

Comments at appropriate places are added to help reader understand the code easily

For exercise 4, non vectorised matrix multiplication for n = 1000 took almost 15 mins, so that part is commented out just for the ease to the viewer running the code on their systems. But the user is free to run that and check that code too!!
